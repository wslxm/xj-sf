package video.simple100;

import org.junit.Test;

import java.util.Arrays;

/***
 * 268. 丢失的数字
 */
public class LeetCode268 {


    @Test
    public void test() {
        //int[] nums = {9, 6, 4, 2, 3, 5, 7, 0, 1};
        // int[] nums = {1, 2};
        int[] nums = {0, 1};
        System.out.println(missingNumber(nums));
    }

    public int missingNumber(int[] nums) {
        // 1、排序
        // 2、依次排队下一个索引的数-上一个索引的数是不是大于1
        Arrays.sort(nums);

        // 情况1，中间缺 0 1 2 3 . 5 6
        for (int i = 0; i < nums.length - 1; i++) {
            int v1 = nums[i];
            int v2 = nums[i + 1];
            if (v2 - v1 > 1) {
                return v1 + 1;
            }
        }
        // 情况2： 1 2  (0-2)  缺第一个0
        // 情况3： 0 1  (0-2)  缺最后一个值
        if (nums[0] != 0) {
            return 0;
        }
        if (nums[nums.length - 1] != nums.length) {
            return nums.length;
        }
        return 0;
    }
}
