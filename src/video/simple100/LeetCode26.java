package video.simple100;

import org.junit.Test;

/**
 *  26. 删除有序数组中的重复项
 */
public class LeetCode26 {

    @Test
    public void test() {
        int[] nums = {0, 0, 1, 1, 1, 2, 2, 3, 3, 4};
        //  int[] nums =  {0,0,1,1,1,2,2,3,3,4};
        //  int[] nums =  {0,1,2,3,4,  2,2,3,3,4};
        System.out.println(removeDuplicates(nums));
    }

    // 关键字：有序数组  原地删除重复
    // 主要操作--去重复(原地)
    public int removeDuplicates(int[] nums) {
        // 索引值
        int index = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[index] != nums[i]) {
                nums[++index] = nums[i];
            }
        }
        return ++index;
    }
}
