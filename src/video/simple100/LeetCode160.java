package video.simple100;

import org.junit.Test;

/***
 * 测试模板
 */
public class LeetCode160 {


    @Test
    public void test() {
        ListNode treeNodeA = new ListNode(1);
        ListNode treeNodeB = new ListNode(2);
        //
        ListNode treeNode3 = new ListNode(3);
        ListNode treeNode4 = new ListNode(4);
        ListNode treeNode5 = new ListNode(5);
        ListNode treeNode6 = new ListNode(6);

        treeNodeA.next = treeNode3;
        treeNodeB.next = treeNode3;
        treeNode3.next = treeNode4;
        treeNode4.next = treeNode5;
        treeNode5.next = treeNode6;

        System.out.println(getIntersectionNode(treeNodeA, treeNodeB).val);
        ;

    }

    public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        // 循环 headA

        while (headA != null) {
            // 循环 headB
            ListNode headBCopy = headB;
            while (headBCopy != null) {
                if (headA == headBCopy) {
                    return headA;
                }
                headBCopy = headBCopy.next;
            }
            headA = headA.next;
        }
        return null;
    }


    public class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
            next = null;
        }
    }
}
