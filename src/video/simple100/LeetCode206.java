package video.simple100;

import org.junit.Test;

/***
 * 206. 反转链表
 */
public class LeetCode206 {


    @Test
    public void test() {
        ListNode listNode1 = new ListNode(1);
        ListNode listNode2 = new ListNode(2);
        ListNode listNode3 = new ListNode(3);
        ListNode listNode4 = new ListNode(4);
        listNode1.next = listNode2;
        listNode2.next = listNode3;
        listNode3.next = listNode4;
        reverseList(listNode1);
    }

    public ListNode reverseList(ListNode head) {
        // 构建新的头节点
        ListNode preNode = null;
        //
        while (head != null) {
            System.out.println(head.val);
            // 添加逻辑
            ListNode node = new ListNode(head.val);
            node.next = preNode;
            preNode = node;
            // 遍历逻辑
            head = head.next;
        }
        return preNode;
    }


    public class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }
}
