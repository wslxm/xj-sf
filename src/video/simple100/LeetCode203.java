package video.simple100;

import org.junit.Test;

/***
 * 203. 移除链表元素
 */
public class LeetCode203 {


    @Test
    public void test() {
        ListNode listNode1 = new ListNode(1);
        ListNode listNode2 = new ListNode(2);
        ListNode listNode3 = new ListNode(3);
        ListNode listNode4 = new ListNode(4);
        listNode1.next  = listNode2;
        listNode2.next  = listNode3;
        listNode3.next  = listNode4;
        removeElements( listNode1, 3);
    }

    public ListNode removeElements(ListNode head, int val) {
        // 头节点
        ListNode newHead = null;
        // 尾节点
        ListNode lastNode = null;
        while (head != null) {
            // 判断当前节点值是否=val，不等于就加入新的链表容器中
            if (head.val != val) {
                if (newHead == null) {
                    // 第一次获取值, 头节点和尾节点都是第一次获取的值
                    newHead = new ListNode(head.val);
                    lastNode = newHead;
                } else {
                    // 非第一次获取值，当前节点就是尾节点
                    lastNode.next = new ListNode(head.val);
                    lastNode = lastNode.next;
                }
            }
            head = head.next;
        }
        return newHead;
    }


    public class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }
}
