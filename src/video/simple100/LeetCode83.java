package video.simple100;


import org.junit.Test;

/***
 * 83. 删除排序链表中的重复元素
 */
public class LeetCode83 {


    @Test
    public void test() {
        ListNode listNode5 = new ListNode(3);
        ListNode listNode4 = new ListNode(3, listNode5);
        ListNode listNode3 = new ListNode(2, listNode4);
        ListNode listNode2 = new ListNode(1, listNode3);
        ListNode listNode1 = new ListNode(1, listNode2);
        //
        ListNode listNode = deleteDuplicates(listNode1);
        System.out.println();
    }


    public ListNode deleteDuplicates(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }
        // 1、遍历
        // 2、取当前节点的值和下一个节点的值
        // 3、判断。相等  下一级的下一级的节点 赋值给 当前节点的下一级
        //         不相等
        // 当前遍历到的节点
        ListNode pNode = head;
        while (pNode.next != null) {
            // 取当前节点的值和下一个节点的值
            Integer nodeVal = pNode.val;
            Integer nextNodeVal = pNode.next.val;
            if (nodeVal.equals(nextNodeVal)) {
                // 把下一级的下一级的节点 赋值给 当前节点的下一级
                pNode.next = pNode.next.next;
            } else {
                pNode = pNode.next;
            }
        }
        return head;
    }


    public class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }
}


//存在一个按升序排列的链表，给你这个链表的头节点 head ，请你删除所有重复的元素，使每个元素 只出现一次 。
//
//        返回同样按升序排列的结果链表。
//
//         
//
//        示例 1：
//
//
//        输入：head = [1,1,2]
//        输出：[1,2]
//        示例 2：
//
//
//        输入：head = [1,1,2,3,3]
//        输出：[1,2,3]
//         
//
//        提示：
//
//        链表中节点数目在范围 [0, 300] 内
//        -100 <= Node.val <= 100
//        题目数据保证链表已经按升序排列
//
//        来源：力扣（LeetCode）
//        链接：https://leetcode-cn.com/problems/remove-duplicates-from-sorted-list
//        著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。