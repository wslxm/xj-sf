package video.simple100;

import org.junit.Test;

import java.util.Arrays;

/***
 * 278. 第一个错误的版本
 */
public class LeetCode283 {


    @Test
    public void test() {
        int[] nums = {0, 1, 0, 3, 12};
        moveZeroes(nums);
        System.out.println(Arrays.toString(nums));
    }


    public void moveZeroes(int[] nums) {
        // 0, 1, 0, 3, 12
        // 1  3  12 0  0
        int zeroCount = 0;
        // 不是0的值前移
        for (int i = 0; i < nums.length; i++) {
            int num = nums[i];
            if (num == 0) {
                zeroCount++;
            } else {
                nums[i - zeroCount] = num;
            }
        }
        // 0移动到最后
        for (int i = 0; i < zeroCount; i++) {
            nums[nums.length - 1 - i] = 0;
        }
    }
}
