package video.simple100;

import org.junit.Test;

/**
 * 实现 strStr()
 */
public class LeetCode28 {

    @Test
    public void test() {
        String haystack = "a";
        String needle = "a";
        System.out.println(strStr(haystack, needle));
    }

    public int strStr(String haystack, String needle) {
        if (haystack.equals("") && needle.equals("")) {
            return 0;
        }
        for (int i = 0; i < haystack.length(); i++) {
            if (i + needle.length() >  haystack.length()) {
                break;
            }
            String one = haystack.substring(i, i + needle.length());
            if (one.equals(needle)) {
                return i;
            }
        }
        return -1;
    }
}
