package video.simple100;

import org.junit.Test;

import java.util.Arrays;

/***
 * 217. 存在重复元素
 */
public class LeetCode217 {


    @Test
    public void test() {

        int[] nums = {1, 2, 3, 2};
        boolean b = containsDuplicate(nums);
        System.out.println(b);
    }


    public boolean containsDuplicate(int[] nums) {
        // 1-先排序
        Arrays.sort(nums);
        for (int i = 0; i < nums.length - 1; i++) {
            if (nums[i] == nums[i + 1]) {
                return true;
            }
        }
        return false;
    }


    // 思路不通

    // 解题思路1 ，获取每一个值出现的次数，判断是不是有大于1，有的话返回true
    // key value
    //  1   2
    //  2   1
    //  3   1

    // 解题思路2 ，循环记录key，如果这个key已经存在过，表示这个值肯定大于1，直接返回true
    //        List<Integer> conutKeys = new ArrayList<>();
    //        for (int i = 0; i < nums.length; i++) {
    //            // 判断是否出现过
    //            if (conutKeys.contains(nums[i])) {
    //                return true;
    //            }
    //            conutKeys.add(nums[i]);
    //        }
    //        return false;

}
