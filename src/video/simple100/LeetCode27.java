package video.simple100;

import org.junit.Test;

/**
 * 27. 移除元素
 */
public class LeetCode27 {

    @Test
    public void test() {
//        输入：nums = [0,1,2,2,3,0,4,2], val = 2
//        输出：5, nums = [0,1,4,0,3]
        int[] nums = {0, 1, 2, 2, 3, 0, 4, 2};
        int val = 3;
        System.out.println(removeElement(nums, val));
    }

    public int removeElement(int[] nums, int val) {
        int index = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != val) {
                nums[index++] = nums[i];
            }
        }
        return index;
    }
}
