package video.simple100;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/***
 * 228. 汇总区间
 */
public class LeetCode228 {


    @Test
    public void test() {
        int[] nums = {-2147483648, -2147483647, 2147483647};
        System.out.println(summaryRanges(nums).toString());
    }


    /**
     * 输入：nums = [0,2,3,4,6,8,9]
     * 输出：["0","2->4","6","8->9"]
     * 解释：区间范围是：
     * [0,0] --> "0"
     * [2,4] --> "2->4"
     * [6,6] --> "6"
     * [8,9] --> "8->9"
     * @param nums
     * @return
     */
    public List<String> summaryRanges(int[] nums) {
        //  解题思路
        // 1、遍历。每次遍历获取 i 的值和 i+1 的值
        // 2、判断  i+1 的值 - i 的值是不是大于1 ,如果是隔断数据
        // 3、如果是隔断数据进行 add （提前创建一个List的容器放返回数据）
        List<String> res = new ArrayList<>();
        // 开始索引,每次隔断重置开始索引
        int startIndex = 0;
        for (int i = 0; i < nums.length; i++) {
            if (i == nums.length - 1) {
                if (startIndex == i) {
                    res.add(nums[startIndex] + "");
                } else {
                    res.add(nums[startIndex] + "->" + nums[i]);
                }
            } else {
                int numi = nums[i];
                int numj = nums[i + 1];
                if (numj - numi != 1) {
                    // 隔断数据
                    if (startIndex == i) {
                        res.add(nums[startIndex] + "");
                    } else {
                        res.add(nums[startIndex] + "->" + numi);
                    }
                    // 重置开始索引
                    startIndex = i + 1;
                }
            }
        }
        return res;
    }

}
