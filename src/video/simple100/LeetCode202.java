package video.simple100;

import org.junit.Test;

import java.util.HashMap;

/***
 * 205. 同构字符串
 */
public class LeetCode202 {


    @Test
    public void test() {
        String s = "paper",
                t = "title";
        System.out.println(isIsomorphic(s, t));
    }

    public boolean isIsomorphic(String s, String t) {
        if (s.length() != t.length()) {
            return false;
        }
        HashMap<String, Integer> map1 = null;
        HashMap<String, Integer> map2 = null;
        String oldAStr = "";
        String oldBStr = "";
        for (int i = 0; i < s.length(); i++) {
            String sIndex = s.charAt(i) + "";
            String tIndex = t.charAt(i) + "";
            if (!oldAStr.equals(sIndex)) {
                map1 = new HashMap();
            }
            if (!oldBStr.equals(tIndex)) {
                map2 = new HashMap();
            }
            oldAStr = sIndex;
            oldBStr = tIndex;
            if (map1.containsKey(sIndex)) {
                map1.put(sIndex, map1.get(sIndex) + 1);
            } else {
                map1.put(sIndex, 1);
            }
            if (map2.containsKey(tIndex)) {
                map2.put(tIndex, map2.get(tIndex) + 1);
            } else {
                map2.put(tIndex, 1);
            }
            //判断是否异同
            if (!map1.get(sIndex).equals(map2.get(tIndex))) {
                return false;
            }
        }
        return true;
    }
}
