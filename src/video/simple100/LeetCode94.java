package video.simple100;


import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/***
 * 94. 二叉树的中序遍历
 * <>
 *     中序遍历 -- 左 中 右
 *     前序遍历 -- 中 左 右
 *     后序遍历 -- 左 右 中
 * </>
 */
public class LeetCode94 {


    @Test
    public void test() {
        TreeNode treeNode = new TreeNode(1, null, new TreeNode(2, new TreeNode(3), null));
        List<Integer> integers = inorderTraversal(treeNode);
        System.out.println();

    }

    public List<Integer> inorderTraversal(TreeNode root) {
        // 定义一个返回对象
        List<Integer> vals = new ArrayList<>();
        if (root == null) {
            return vals;
        }
        // 进行递归获取遍历顺序的数据
        nextNode(root, vals);
        return vals;
    }

    public void nextNode(TreeNode root, List<Integer> vals) {
        //   - 1、判断是不是有左节点，有的话对继续进行递归
        if (root.left != null) {
            nextNode(root.left, vals);
        }
        //   - 2、没有左字点或者左节点已经获取玩了，直接获取中间的值
        vals.add(root.val);
        //   - 3、判断是不是有右节点，有的话对继续进行递归
        if (root.right != null) {
            nextNode(root.right, vals);
        }
    }


    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
}


//    给定一个二叉树的根节点 root ，返回 它的 中序 遍历 。
//
//         
//
//        示例 1：
//
//
//        输入：root = [1,null,2,3]
//        输出：[1,3,2]
//        示例 2：
//
//        输入：root = []
//        输出：[]
//        示例 3：
//
//        输入：root = [1]
//        输出：[1]
//         
//
//        提示：
//
//        树中节点数目在范围 [0, 100] 内
//        -100 <= Node.val <= 100
//         
//
//        来源：力扣（LeetCode）
//        链接：https://leetcode-cn.com/problems/binary-tree-inorder-traversal
//        著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。