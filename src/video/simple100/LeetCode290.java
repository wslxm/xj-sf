package video.simple100;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/***
 * 290. 单词规律
 */
public class LeetCode290 {


    @Test
    public void test() {
        String pattern = "abba", s = "cat dog dog cat";
        System.out.println(wordPattern(pattern, s));
        ;
    }


    public boolean wordPattern(String pattern, String s) {
        String[] sArr = s.split(" ");
        if(pattern.length() != sArr.length){
            return false;
        }
        Map<Character, String> map = new HashMap<>();
        Map<String, Character> map2 = new HashMap<>();
        for (int i = 0; i < pattern.length(); i++) {
            char p = pattern.charAt(i);
            String s1 = sArr[i];
            if (map.containsKey(p) && !map.get(p).equals(s1)) {
                return false;
            }
            if (map2.containsKey(s1) && map2.get(s1) != p) {
                return false;
            }
            map.put(p, s1);
            map2.put(s1, p);
        }
        return true;
    }
}
