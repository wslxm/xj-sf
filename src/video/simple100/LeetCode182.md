182. 查找重复的电子邮箱
```sql
select 
distinct t1.Email   
from Person t1
left join Person t2 on t1.Email = t2.Email and t1.id != t2.id
where t2.Email is not null
```