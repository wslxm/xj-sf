package video.simple100;

import org.junit.Test;

/***
 * 258. 各位相加
 */
public class LeetCode263 {


    @Test
    public void test() {
        System.out.println(isUgly(14));
    }


    //   50 /5  = 10
    //    10 /5 = 2
    //    2  /2 = 1
    //    TRUE
    //
    //    33 / 3 = 11
    //    11
    //    FALSE
    public boolean isUgly(int n) {   // 999;
        // 判断n是不是能除 2 3  5 这个数
        // 7
        while (n > 1) {
            if (n % 5 == 0) {
                // 10 & 5 = 0  | 11 & 5 = 1  | 12 & 5 = 2
                n = n / 5;
            } else if (n % 3 == 0) {
                n = n / 3;
            } else if (n % 2 == 0) {
                n = n / 2;
            } else {
                break;
            }
        }
        return n == 1;
    }
}
