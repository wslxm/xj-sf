package video.simple100;

import org.junit.Test;

import java.util.Stack;

/**
 * 20. 有效的括号
 */
public class LeetCode20 {

    @Test
    public void test() {
        String s = "]";
        System.out.println(isValid(s));
    }

    public boolean isValid(String s) {
        Stack<Character> stack = new Stack();
        for (int i = 0; i < s.length(); i++) {
            if ('{' == s.charAt(i)) {
                stack.push('}');
            } else if ('(' == s.charAt(i)) {
                stack.push(')');
            } else if ('[' == s.charAt(i)) {
                stack.push(']');
            } else {
                //反括号
                char c = s.charAt(i);
                if (stack.isEmpty() || c != stack.pop()) {
                    return false;
                }
            }
        }
        return stack.isEmpty();
    }
}
