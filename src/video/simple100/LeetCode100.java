package video.simple100;


import org.junit.Test;

/***
 * 100. 相同的树
 */
public class LeetCode100 {


    @Test
    public void test() {
        TreeNode treeNode1 = new TreeNode(1, null,new TreeNode(2) );
        TreeNode treeNode2 = new TreeNode(1, null, new TreeNode(2));
        System.out.println(isSameTree(treeNode1, treeNode2));

    }

    public boolean isSameTree(TreeNode p, TreeNode q) {
        StringBuffer sb1 = new StringBuffer();
        StringBuffer sb2 = new StringBuffer();
        nextNode(p, sb1);
        nextNode(q, sb2);
        return sb1.toString().equals(sb2.toString());
    }


    public void nextNode(TreeNode root, StringBuffer sb) {
        if (root == null) {
            return;
        } else {
            sb.append(root.val).append("-");
        }

        //   - 1、判断是不是有左节点，有的话对继续进行递归
        if (root.left != null) {
            nextNode(root.left, sb);
        } else {
            sb.append("-");
        }

        //   - 3、判断是不是有右节点，有的话对继续进行递归
        if (root.right != null) {
            nextNode(root.right, sb);
        } else {
            sb.append("-");
        }

    }


    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
}

//100. 相同的树
//        给你两棵二叉树的根节点 p 和 q ，编写一个函数来检验这两棵树是否相同。
//
//        如果两个树在结构上相同，并且节点具有相同的值，则认为它们是相同的。
//
//
//
//        示例 1：
//
//
//        输入：p = [1,2,3], q = [1,2,3]
//        输出：true
//        示例 2：
//
//
//        输入：p = [1,2], q = [1,null,2]
//        输出：false
//        示例 3：
//
//
//        输入：p = [1,2,1], q = [1,1,2]
//        输出：false
//