package video.simple100;

import org.junit.Test;

/**
 * 35. 搜索插入位置
 */
public class LeetCode35 {

    @Test
    public void test() {
        int[] nums = {1, 3, 5, 6};
        int target = 2;
        System.out.println(searchInsert(nums, target));
    }

    public int searchInsert(int[] nums, int target) {
        int index = 0;
        for (int i = 0; i < nums.length; i++) {
            // 1、找索引
            if (nums[i] == target) {
                return i;
            }
            // 2、找插入位置
            if (target >= nums[i]) {
                index = i + 1;
            }
        }
        return index;
    }
}
