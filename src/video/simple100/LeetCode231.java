package video.simple100;

import org.junit.Test;

/***
 * 231. 2 的幂
 */
public class LeetCode231 {


    @Test
    public void test() {
        System.out.println(isPowerOfTwo(3));
    }


    public boolean isPowerOfTwo(int n) {
        double nd = (double) n;
        while (nd >= 1) {
            if (nd == 2 || nd == 1) {
                return true;
            }
            nd = nd / 2;
        }
        return false;
    }
}
