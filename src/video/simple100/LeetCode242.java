package video.simple100;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/***
 * 242. 有效的字母异位词
 */
public class LeetCode242 {


    @Test
    public void test() {
        String s = "anagram", t = "nagaramr";
        System.out.println(isAnagram(s, t));
    }

    public boolean isAnagram(String s, String t) {
        if (s.length() != t.length()) {
            return false;
        }
        // 1、把数据放到 list 集合
        // 2、排序
        // 3、对比
        List<Character> s1 = new ArrayList<>();
        List<Character> t1 = new ArrayList<>();
        for (int i = 0; i < s.length(); i++) {
            s1.add(s.charAt(i));
            t1.add(t.charAt(i));
        }
        Collections.sort(s1);
        Collections.sort(t1);
        return s1.toString().equals(t1.toString());
    }
}
