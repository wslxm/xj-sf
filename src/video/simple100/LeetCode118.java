package video.simple100;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/***
 * 118. 杨辉三角
 */
public class LeetCode118 {


    @Test
    public void test() {
        System.out.println(  generate(5));
    }

    public List<List<Integer>> generate(int numRows) {
        List<List<Integer>> resList = new ArrayList<>();
        int row = 1;
        while (row <= numRows) {
            // 生成行
            List<Integer> rowList = new ArrayList<>();
            // 生成行的每一个数据
            for (int i = 0; i < row; i++) {
                if (i == 0 || i == row - 1) {
                    // 生成行的第一个数据和最后数据数据 固定值 1
                    rowList.add(1);
                } else {
                    // 生成行的每一个中间的数据，= 上一行的当前索引值 + 当前索引减一的值的数据
                    // 当前行索引= row-1， 取上一行的索引减-1，在-1
                    List<Integer> qRow = resList.get(row - 2);
                    rowList.add(qRow.get(i - 1) + qRow.get(i));
                }
            }
            resList.add(rowList);
            row++;
        }
        return resList;
    }
}


//给定一个非负整数 numRows，生成「杨辉三角」的前 numRows 行。
//
//        在「杨辉三角」中，每个数是它左上方和右上方的数的和。
//
//
//
//         
//
//        示例 1:
//
//        输入: numRows = 5
//        输出: [[1],[1,1],[1,2,1],[1,3,3,1],[1,4,6,4,1]]
//        示例 2:
//
//        输入: numRows = 1
//        输出: [[1]]
//         
//
//        提示:
//
//        1 <= numRows <= 30
//
//        来源：力扣（LeetCode）
//        链接：https://leetcode-cn.com/problems/pascals-triangle
//        著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。