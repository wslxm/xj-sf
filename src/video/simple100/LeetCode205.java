package video.simple100;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/***
 * 205. 同构字符串
 */
public class LeetCode205 {


    @Test
    public void test() {
        System.out.println(isIsomorphic("eggrre", "addttd"));
        ;
    }


    public boolean isIsomorphic(String s, String t) {
        Map<String, String> s1 = new HashMap<>();
        Map<String, String> t1 = new HashMap<>();
        for (int i = 0; i < s.length(); i++) {
            String sVal = s.charAt(i) + "";
            String tVal = t.charAt(i) + "";
            if (s1.containsKey(sVal) && !s1.get(sVal).equals(tVal)) {
                return false;
            }
            if (t1.containsKey(tVal) && !t1.get(tVal).equals(sVal)) {
                return false;
            }
            s1.put(sVal, tVal);
            t1.put(tVal, sVal);
            // System.out.println(s1.toString());
            // System.out.println(t1.toString());
            // System.out.println("===========");
        }
        return true;
    }
}
