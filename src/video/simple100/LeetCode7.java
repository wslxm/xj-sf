package video.simple100;

import org.junit.Test;

/**
 * 7. 整数反转
 */
public class LeetCode7 {

    @Test
    public void test() {
        int x = -1566859598;
        System.out.println(reverse(x));
    }


    public int reverse(int x) {

        // 1、判断是否为负数
        String xstr = x + "";
        String fs = "";
        if (xstr.substring(0, 1).equals("-")) {
            fs = "-";
            xstr = xstr.substring(1);
        }

        // 2、进行反转
        String res = "";
        for (int i = xstr.length() - 1; i >= 0; i--) {
            res += xstr.charAt(i);
        }

        // 3、返回反转后的结果
        try {
            return Integer.parseInt(fs + res);
        }catch (Exception e){
            return 0;
        }
    }
}
