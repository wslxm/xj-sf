197. 上升的温度
```sql

select 
t1.id
from Weather t1
left join Weather t2 on dateDiff(t1.recordDate,t2.recordDate) = 1
where t1.Temperature > t2.Temperature

```