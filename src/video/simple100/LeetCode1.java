package video.simple100;

import org.junit.Test;

import java.util.Arrays;

/**
 * 两数之和
 */
public class LeetCode1 {

    @Test
    public void test() {
        int[] nums = {1, 10, 21, 7};
        int target = 28;
        System.out.println(Arrays.toString(twoSum(nums, target)));
    }


    public int[] twoSum(int[] nums, int target) {
        for (int i = 0; i < nums.length; i++) {
            for (int j = 0; j < nums.length; j++) {
                if (i != j && nums[i] + nums[j] == target) {
                    return new int[]{i, j};
                }
            }
        }
        return null;
    }
}
