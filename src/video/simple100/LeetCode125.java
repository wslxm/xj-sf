package video.simple100;

import org.junit.Test;

/***
 * 125. 验证回文串
 */
public class LeetCode125 {


    @Test
    public void test() {
        System.out.println(isPalindrome("AcA"));
    }

    public boolean isPalindrome(String s) {
        // 第1步，去符号
        StringBuffer nesStr = new StringBuffer();
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            int cAscii = (int) c;
            // 48-57(数字) |  65-90(大写字母) |  97-122(小写字母)
            if ((cAscii >= 48 && cAscii <= 57)
                    || (cAscii >= 65 && cAscii <= 90)
                    || (cAscii >= 97 && cAscii <= 122)
            ) {
                nesStr.append(c);
            }
        }
        // 第2步，转小写
        String s1 = nesStr.toString().toLowerCase();
        //System.out.println(s1);
        // 第3步，判断是否是回文（索引开始位 和 结束位 往中间依次判断是否相同）
        for (int i = 0; i < s1.length() / 2; i++) {
            if (s1.charAt(i) != s1.charAt(s1.length() - 1 - i)) {
                return false;
            }
        }
        return true;
    }
}


//
//给定一个字符串，验证它是否是回文串，只考虑字母和数字字符，可以忽略字母的大小写。
//
//        说明：本题中，我们将空字符串定义为有效的回文串。
//
//         
//
//        示例 1:
//
//        输入: "A man, a plan, a canal: Panama"
//        输出: true
//        解释："amanaplanacanalpanama" 是回文串
//        示例 2:
//
//        输入: "race a car"
//        输出: false
//        解释："raceacar" 不是回文串
//         
//
//        提示：
//
//        1 <= s.length <= 2 * 105
//        字符串 s 由 ASCII 字符组成
//
//        来源：力扣（LeetCode）
//        链接：https://leetcode-cn.com/problems/valid-palindrome
//        著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。