package video.simple100;

import org.junit.Test;

import java.util.ArrayList;

/***
 * 136. 只出现一次的数字
 */
public class LeetCode136 {


    @Test
    public void test() {
        int[] nums = {1, 2,  1, 2,4};
        System.out.println(singleNumber(nums)); ;
    }


    public int singleNumber(int[] nums) {
        ArrayList<Integer> list = new ArrayList();
        for (int i = 0; i < nums.length; i++) {
            list.add(nums[i]);
        }
        // 1、找到有重复的数据，就把俩个值踢出去
        Integer resNum = null;
        while (list.size() >= 3 && resNum == null) {
            for (int i = 0; i < list.size(); i++) {
                boolean isBack = false;
                for (int j = i + 1; j < list.size(); j++) {
                    if (list.get(i).equals(list.get(j))) {
                        list.remove(j);
                        list.remove(i);
                        isBack = true;
                        break;
                    }
                }
                if (isBack) {
                    break;
                } else {
                    // 提前找到
                    resNum = list.get(i);
                    break;
                }
            }
        }
        if (resNum == null) {
            resNum = list.get(0);
        }
        return resNum;
    }
}

//
//给定一个非空整数数组，除了某个元素只出现一次以外，其余每个元素均出现两次。找出那个只出现了一次的元素。
//
//        说明：
//
//        你的算法应该具有线性时间复杂度。 你可以不使用额外空间来实现吗？
//
//        示例 1:
//
//        输入: [2,2,1]
//        输出: 1
//        示例 2:
//
//        输入: [4,1,2,1,2]
//        输出: 4
//
//        来源：力扣（LeetCode）
//        链接：https://leetcode-cn.com/problems/single-number
//        著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
