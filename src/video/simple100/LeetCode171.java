package video.simple100;

import org.junit.Test;

/***
 * 171. Excel 表列序号
 */
public class LeetCode171 {


    @Test   // 26  (26*x)
    public void test() {
        System.out.println(titleToNumber("BXZY"));
    }

    public int titleToNumber(String columnTitle) {
        Integer resNum = 0;
        for (int i = 0; i < columnTitle.length(); i++) {
            // 1、先获取字母大小 （int）  A-Z = 1-26
            // b   x*26*26*26   (x当前charNum大小)
            int charNum = (char) columnTitle.charAt(i) - 'A' + 1;
            // 2、获取每一个字母的值
            for (int j = 0; j < columnTitle.length() - i - 1; j++) {
                charNum *= 26;
            }
            // 3、每个字母的值进行相加
            resNum += charNum;
        }
        return resNum;
    }
}
