package video.simple100;

import org.junit.Test;

/**
 * 14. 最长公共前缀
 */
public class LeetCode14 {

    @Test
    public void test() {
        String[] strs = {"flower", "f1xx", "flight"};
        System.out.println(longestCommonPrefix(strs));
    }


    public String longestCommonPrefix(String[] strs) {

        // 判空
        if (strs == null || strs.length == 0) {
            return "";
        }

        // 1、获取所有的可能性，strs[0] | 拆解 flower  -> f  f1 flo flow flowe flower
        String[] strsknx = new String[strs[0].length()];
        for (int i = 0; i < strs[0].length(); i++) {
            strsknx[i] = strs[0].substring(0, i + 1);
        }

        // 2、-> 遍历这个可能性, 同时 -> 遍历strs的参数获取到和当前拆解的长度为一致的字符串
        String result = "";
        for (int i = 0; i < strsknx.length; i++) {
            // 可能性参数, len
            String val1 = strsknx[i];
            // 判断
            boolean isVal = true;
            for (int j = 0; j < strs.length; j++) {
                // 判断当前参数长度是否比可能性参数长度低或者相同
                if(strs[j].length() < val1.length()){
                    isVal= false;
                    break;
                }

                // 获取和可能性相同长度的参数
                String val2 = strs[j].substring(0, val1.length());
                if (!val1.equals(val2)) {
                    isVal = false;
                    break;
                }
            }
            // 结果处理
            if(!isVal){
                break;
            }else{
                result = val1;
            }
        }
        return result;
        // 找到不同就跳出来, 遍历可能性把当前可能的直接设置模拟最终值
    }
}
