package video.simple100;

import org.junit.Test;

/***
 * 168. Excel表列名称
 */
public class LeetCode168 {


    @Test
    public void test() {
        System.out.println(convertToTitle(701));
    }

    public String convertToTitle(int columnNumber) {
        String str = "";
        while (columnNumber > 0) {
            columnNumber--;
            int num = columnNumber % 26;
            char z = (char) ('A' + num);
            str = z + str;
            columnNumber = columnNumber / 26;
        }
        return str;
    }
}
