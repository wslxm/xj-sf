package video.simple100;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/***
 * 119. 杨辉三角 II
 */
public class LeetCode119 {


    @Test
    public void test() {
        System.out.println(getRow(3));
    }

    public List<Integer> getRow(int rowIndex) {
        List<List<Integer>> resList = new ArrayList<>();
        int row = 0;
        while (row <= rowIndex) {
            // 生成行
            List<Integer> rowList = new ArrayList<>();
            // 行数据是1开始的，但是我们row初始是0，所以要加1
            int dRow = row + 1;
            // 生成行的每一个数据
            for (int i = 0; i < dRow; i++) {
                if (i == 0 || i == dRow - 1) {
                    // 生成行的第一个数据和最后数据数据 固定值 1
                    rowList.add(1);
                } else {
                    // 生成行的每一个中间的数据，= 上一行的当前索引值 + 当前索引减一的值的数据
                    // 当前行索引= row-1， 取上一行的索引减-1，在-1
                    List<Integer> qRow = resList.get(dRow - 2);
                    rowList.add(qRow.get(i - 1) + qRow.get(i));
                }
            }
            resList.add(rowList);
            row++;
        }
        return resList.get(resList.size() - 1);
    }
}


//    给定一个非负索引 rowIndex，返回「杨辉三角」的第 rowIndex 行。
//
//        在「杨辉三角」中，每个数是它左上方和右上方的数的和。
//
//
//
//         
//
//        示例 1:
//
//        输入: rowIndex = 3
//        输出: [1,3,3,1]
//        示例 2:
//
//        输入: rowIndex = 0
//        输出: [1]
//        示例 3:
//
//        输入: rowIndex = 1
//        输出: [1,1]
//         
//
//        提示:
//
//        0 <= rowIndex <= 33
//
//        来源：力扣（LeetCode）
//        链接：https://leetcode-cn.com/problems/pascals-triangle-ii
//        著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。