package video.simple100;


import org.junit.Test;

/***
 * 101. 对称二叉树
 */
public class LeetCode101 {


    @Test
    public void test() {
        //         1
        //       2   2
        //     3  4 4 3
//        TreeNode treeNode = new TreeNode(1,
//                new TreeNode(2, new TreeNode(3), new TreeNode(4)),
//                new TreeNode(2, new TreeNode(4), new TreeNode(3))
//        );

        TreeNode treeNode = new TreeNode(1,
                new TreeNode(2, null, new TreeNode(3)),
                new TreeNode(2, null, new TreeNode(3))
        );
        System.out.println(isSymmetric(treeNode)); ;
    }

    public boolean isSymmetric(TreeNode root) {
        // 1、遍历左节点获取左节点的数据 （遍历方式，1-中 2-左边 3-右边）
        // 2、遍历右节点获取右节点的数据 （遍历方式，1-中 2-右边 3-左边）
        // 3、判断左节点 右节点遍历出来的数据是否一致
        StringBuffer sb1 = new StringBuffer();
        StringBuffer sb2 = new StringBuffer();
        nextNode(root.left, sb1, 1);
        nextNode(root.right, sb2, 2);
        return sb1.toString().equals(sb2.toString());
    }


    /**
     *
     * @param root
     * @param sb
     * @param type 1= 左遍历  2-右遍历
     *          type  1 -（遍历方式，1-中 2-左边 3-右边）
     *          type  2 -（遍历方式，1-中 2-右边 3-左边）
     * @return
     */
    public void nextNode(TreeNode root, StringBuffer sb, int type) {
        // 获取中间节点
        if (root == null) {
            return;
        } else {
            sb.append(root.val).append("-");
        }
        if (type == 1) {
            //  判断是不是有左节点，有的话对继续进行递归
            if (root.left != null) {
                nextNode(root.left, sb, type);
            } else {
                sb.append("-");
            }
            //  判断是不是有右节点，有的话对继续进行递归
            if (root.right != null) {
                nextNode(root.right, sb, type);
            } else {
                sb.append("-");
            }
        } else {
            //  判断是不是有右节点，有的话对继续进行递归
            if (root.right != null) {
                nextNode(root.right, sb, type);
            } else {
                sb.append("-");
            }
            //  判断是不是有左节点，有的话对继续进行递归
            if (root.left != null) {
                nextNode(root.left, sb, type);
            } else {
                sb.append("-");
            }
        }
    }


    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
}


//    给你一个二叉树的根节点 root ， 检查它是否轴对称。
//
//         
//
//        示例 1：
//
//
//        输入：root = [1,2,2,3,4,4,3]
//        输出：true
//        示例 2：
//
//
//        输入：root = [1,2,2,null,3,null,3]
//        输出：false
//         
//
//        提示：
//
//        树中节点数目在范围 [1, 1000] 内
//        -100 <= Node.val <= 100
//
//        来源：力扣（LeetCode）
//        链接：https://leetcode-cn.com/problems/symmetric-tree
//        著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。