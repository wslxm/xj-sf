package video.simple100;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * 155. 最小栈
 * MinStack() 初始化堆栈对象。
 * void push(int val) 将元素val推入堆栈。
 * void pop() 删除堆栈顶部的元素。
 * int top() 获取堆栈顶部的元素。
 * int getMin() 获取堆栈中的最小元素。
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/min-stack
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @author wangsong
 * @mail 1720696548@qq.com
 * @date 2022/4/13 0013 20:18
 * @version 1.0.0
 */
public class LeetCode155 {


    @Test
    public void test() {
        MinStack minStack = new MinStack();
        minStack.push(-2);
        minStack.push(0);
        minStack.push(-3);
        System.out.println(minStack.getMin());  // --> 返回 -3.
        minStack.pop();
        System.out.println(minStack.top());     // --> 返回 0.
        System.out.println(minStack.getMin());   //--> 返回 -2.
        System.out.println(minStack.getMin());   //--> 返回 -2.
        System.out.println(minStack.getMin());   //--> 返回 -2.
        System.out.println(minStack.getMin());   //--> 返回 -2.
    }


    class MinStack {

        List<Integer> stack;
        Integer minC = null;

        public MinStack() {
            stack = new ArrayList<>();
        }

        public void push(int val) {
            stack.add(val);
            minC = null;
        }

        public void pop() {
            //  6
            //  5
            //  6
            //  4
            //  1
            stack.remove(stack.size() - 1);
            minC = null;
        }

        public int top() {
            return stack.get(stack.size() - 1);
        }

        public int getMin() {
            if (minC == null) {
                Integer min = stack.get(0);
                for (int i = 0; i < stack.size(); i++) {
                    if (min > stack.get(i)) {
                        min = stack.get(i);
                    }
                }
                minC = min;
            }
            return minC;
        }
    }
}
