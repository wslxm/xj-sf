package video.simple100;

import org.junit.Test;

/**
 * 69. x 的平方根
 */
public class LeetCode69 {

    @Test
    public void test() {
        int x = 2147483647;
        System.out.println(mySqrt(x));

    }


    /**
     *
     */
    public int mySqrt(int x) {
        int l = 0;  // 左边界
        int r = x;  //右边界
        int n = -1; // 临近值（最后的结果）
        while (r >= l) {
            int mid = l + (r - l) / 2;
            if ((long)mid * mid > x) {
                // 因为当前mid已经验证过啦，所以mid-1
                r = mid - 1;
            } else {
                n = mid;
                l = l + 1;
            }
        }
        return n;
    }
}
