package video.simple100;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/***
 * 160. 相交链表
 */
public class LeetCode144 {


    @Test
    public void test() {
        TreeNode treeNode = new TreeNode(1,
                null,
                new TreeNode(2,
                        new TreeNode(3),
                        null));
        System.out.println(preorderTraversal(treeNode));
    }

    public List<Integer> preorderTraversal(TreeNode root) {
        List<Integer> resList = new ArrayList<>();
        nextNode(root, resList);
        return resList;
    }


    /***
     * 递归遍历    // 前序遍历 -- 中 左 右
     * @author wangsong
     * @param root
     * @param resList
     * @date 2022/4/13 0013 19:56
     * @return void
     * @version 1.0.0
     */
    void nextNode(TreeNode root, List<Integer> resList) {
        if (root == null) {
            return;
        }
        // 1、获取中间值
        resList.add(root.val);
        // 2、获取左边的值
        nextNode(root.left, resList);
        // 3、获取右边的值
        nextNode(root.right, resList);
    }


    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
}
