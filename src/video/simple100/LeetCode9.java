package video.simple100;

import org.junit.Test;

/**
 * 9. 回文数
 */
public class LeetCode9 {

    @Test
    public void test() {
        int x = 101;
        System.out.println(isPalindrome(x));
    }


    public boolean isPalindrome(int x) {
        // 1、判断是否小于0
        if (x < 0) {
            return false;
        }
        // 获取比较次数
        String xstr = x + "";
        int i = xstr.length() / 2;
        for (int j = 0; j < i; j++) {
            // 开始比较
            char v1 = xstr.charAt(j);
            char v2 = xstr.charAt(xstr.length() - 1 - j);
            if (v1 != v2) {
                return false;
            }
        }
        return true;
    }
}
