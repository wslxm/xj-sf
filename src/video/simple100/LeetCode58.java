package video.simple100;

import org.junit.Test;

/**
 * 58. 最后一个单词的长度
 */
public class LeetCode58 {

    @Test
    public void test() {
        String s = " He  llo  Wo  rld     ";
        System.out.println(lengthOfLastWord(s));
    }


    /**
     * 解题思路
     *  --- 从后面开始遍历，判空，获取到最后一个单词的就返回结果
     *    //  s = "Hello World"
     *    //  s = " Hello World  "
     *    //  s = " He  llo  Wo  rld     "
     *    //  s = "      "
     * @param s
     * @return
     */
    public int lengthOfLastWord(String s) {
        int len = 0;
        for (int i = s.length() - 1; i >= 0; i--) {
            if (s.charAt(i) != ' ') {
                // 最后一个单词的每一个值都会获取到
                len++;
            } else {
                if (len != 0) {
                    // 最后一个单词获取完了
                    return len;
                }
            }
        }
        return len;
    }
}
