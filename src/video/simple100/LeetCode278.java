package video.simple100;

import org.junit.Test;

/***
 * 278. 第一个错误的版本
 */
public class LeetCode278 {


    @Test
    public void test() {
//        VersionControl versionControl = new VersionControl();
//        System.out.println(versionControl.isBadVersion(3));
//        System.out.println(versionControl.isBadVersion(5));
//        System.out.println(versionControl.isBadVersion(4));
        Solution solution = new Solution();
        System.out.println(solution.firstBadVersion(9));

    }

    public class VersionControl {
        int bad = 4;

        boolean isBadVersion(int version) {
            return version >= bad;
        }
    }


    public class Solution extends VersionControl {
        public int firstBadVersion(int n) {
            int l = 0;
            int r = n;
            while (true) {
                int mid = l + ((r - l) / 2);
                // false 往左边  true 往右边
                if (isBadVersion(mid)) {
                    r = mid;
                } else {
                    l = mid;
                }
                if (r - l <= 1) {
                    break;
                }
            }
            // 示例1 l=3 r=4
            // 示例2 l=4 r=5
            // 示例1和2返回肯定是 4
            return isBadVersion(l) ? l : r;
        }
    }
}
