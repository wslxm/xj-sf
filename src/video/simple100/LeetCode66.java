package video.simple100;

import org.junit.Test;

import java.util.Arrays;

/**
 * 66/加一
 */
public class LeetCode66 {

    @Test
    public void test() {
        int[] digits = {9, 9, 9, 9};
        System.out.println(Arrays.toString(plusOne(digits)));
    }


    //   情况1   1 2 4 5    (对最后一位进行+1)
    //   情况2   1299       (对最后一位进行+1，在判断是否需要进位，9+1=10 需要进位)
    //   情况3   9999 + 1   （长度由4变成5）
    public int[] plusOne(int[] digits) {
        // 情况1
        if (digits[digits.length - 1] != 9) {
            digits[digits.length - 1]++;
            return digits;
        }
        // 情况2
        for (int i = digits.length - 1; i >= 0; i--) {
            if (digits[i] == 9) {
                // 进位
                digits[i] = 0;
            } else {
                // 一但不为9，就会到这来
                System.out.println("索引" + i + " = " + digits[i]);
                digits[i] ++;
                return digits;
            }
        }
        //  情况3,直接在情况2的基础上操作
        if(digits[0] == 0){
            digits = new int[digits.length + 1];
            digits[0] = 1;
        }
        return digits;
    }
}
