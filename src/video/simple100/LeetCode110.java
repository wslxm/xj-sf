package video.simple100;


import org.junit.Test;

/***
 * 110. 平衡二叉树
 */
public class LeetCode110 {


    @Test
    public void test() {
        TreeNode root = new TreeNode(4,
                new TreeNode(3, new TreeNode(2, new TreeNode(1), null), null),
                new TreeNode(5, null, new TreeNode(6, null, new TreeNode(7)))
        );
        System.out.println(isBalanced(root));
        ;
    }


    public boolean isBalanced(TreeNode root) {
        // 获取每一层 左边的深度和右边的深度做比较

        // result[0] = 0 是平衡高度的二叉树  =1 他不是平衡高度的二叉树
        int[] result = {0};
        maxDepth(root, result);
        return result[0] == 0;
    }


    public int maxDepth(TreeNode root, int[] result) {
        if (result[0] == 1) {
            return -1;
        }
        //
        if (root == null) {
            return 0;
        }
        // 左边最大值
        int leftMax = maxDepth(root.left, result);
        // 右边最大值
        int rightMax = maxDepth(root.right, result);
        //
        if (leftMax - rightMax > 1 || rightMax - leftMax > 1) {
            result[0] = 1;
            return -1;
        }
        // 获取左边或右边  深度大的值
        int maxNum = leftMax > rightMax ? leftMax : rightMax;
        return maxNum + 1;
    }


    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
}
//给定一个二叉树，判断它是否是高度平衡的二叉树。
//
//        本题中，一棵高度平衡二叉树定义为：
//
//        一个二叉树每个节点 的左右两个子树的高度差的绝对值不超过 1 。
//
//         
//
//        示例 1：
//
//
//        输入：root = [3,9,20,null,null,15,7]
//        输出：true
//        示例 2：
//
//
//        输入：root = [1,2,2,3,3,null,null,4,4]
//        输出：false
//        示例 3：
//
//        输入：root = []
//        输出：true
//         
//
//        提示：
//
//        树中的节点数在范围 [0, 5000] 内
//        -104 <= Node.val <= 104
//
//        来源：力扣（LeetCode）
//        链接：https://leetcode-cn.com/problems/balanced-binary-tree
//        著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。