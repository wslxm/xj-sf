package video.simple100;

import org.junit.Test;

/***
 * 234. 回文链表
 */
public class LeetCode234 {


    @Test
    public void test() {

        ListNode listNode1 = new ListNode(1);
        ListNode listNode2 = new ListNode(2);
        ListNode listNode3 = new ListNode(2);
        ListNode listNode4 = new ListNode(2);
        // ListNode listNode5 = new ListNode();
        listNode1.next = listNode2;
        listNode2.next = listNode3;
        listNode3.next = listNode4;
        System.out.println(isPalindrome(listNode1));
    }

    public boolean isPalindrome(ListNode head) {
        // 1、遍历数据获取所有值放入到 string
        // 2、折中遍历
        StringBuffer sb = new StringBuffer();
        while (head != null) {
            sb.append(head.val);
            head = head.next;
        }
        // 中间索引  12  |  21
        //              3
        int zIndex = sb.length() / 2;
        String sbStr = sb.toString();
        for (int i = 0; i < zIndex; i++) {
            char l = sbStr.charAt(i);
            char r = sbStr.charAt(sb.length() - i - 1);
            if (l != r) {
                return false;
            }
        }
        return true;
    }


    public class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }
}
