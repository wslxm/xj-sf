package video.simple100;

import org.junit.Test;

/**
 * 66/加一
 *
 *
 *      计算公式：
 *        根据“逢二进一”规则，二进制数加法的法则为：
 *        0＋0＝0                 0
 *        0＋1＝1＋0＝1            1
 *        1＋1＝0　（进位为1）      2
 *        1＋1＋1＝1 （进位为1）    3
 *     ---------------------------
 *         1010
 *         1011     ---> 10101
 *        1 1
 *         ----
 *        10101
 */
public class LeetCode67 {

    @Test
    public void test() {
        String a = "11";
        String b = "1";
        System.out.println(addBinary(a, b));
    }


    /**
     *  解题思路：
     *  1、处理位数，让a和b位数保持一致
     *  2、根据计算公式进行计算
     */
    public String addBinary(String a, String b) {
        //
        if (a.length() >= b.length()) {
            // 修改b的长度 3 - 1  = 2
            int mt = a.length() - b.length();
            for (int i = 0; i < mt; i++) {
                b = "0" + b;
            }
        } else {
            // 修改a的长度
            int mt = b.length() - a.length();
            for (int i = 0; i < mt; i++) {
                a = "0" + a;
            }
        }
        // 计算
        int[] cArr = new int[a.length()];
        int jNum = 0;
        for (int i = a.length() - 1; i >= 0; i--) {
            int aval = Integer.parseInt(a.charAt(i) + "");
            int bval = Integer.parseInt(b.charAt(i) + "");
            // 计算值
            int val = jNum + aval + bval;
            // 判断是否进位
            jNum = (val >= 2) ? 1 : 0;
            // 当前位数的结果
            cArr[i] = (val == 0 || val == 2) ? 0 : 1;
        }

        // 处理结果（如果jNum 最后还存在进位值，直接在最前方进行补1）
        String cStr = (jNum == 1) ? "1" : "";
        for (int i = 0; i < cArr.length; i++) {
            cStr += cArr[i];
        }
        return cStr;
    }
}
