package video.simple100;


import org.junit.Test;

/***
 * 111. 二叉树的最小深度
 */
public class LeetCode111 {


    @Test
    public void test() {
        TreeNode root = new TreeNode(3,
                //new TreeNode(9),
                null,
                new TreeNode(20, new TreeNode(15), new TreeNode(7))
        );
        System.out.println(minDepth(root));
    }

    public int minDepth(TreeNode root) {
        if (root == null) {
            return 0;
        }
        // 往左边递归
        int leftMax = minDepth(root.left);
        // 往右边边递归
        int rightMax = minDepth(root.right);
        //
        if (root.left == null && root.right == null) {
            // 俩边都是null  （返回当前节点的深度 1）
            return 1;
        } else if (root.left != null && root.right != null) {
            // 俩边都有子节点 （返回当前深度小的值）
            return leftMax > rightMax ? rightMax + 1 : leftMax + 1;
        } else {
            // 俩边有一边有子节点  （返回当前深度大的值）
            return leftMax > rightMax ? leftMax + 1 : rightMax + 1;
        }
    }


    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
}
//给定一个二叉树，找出其最小深度。
//
//        最小深度是从根节点到最近叶子节点的最短路径上的节点数量。
//
//        说明：叶子节点是指没有子节点的节点。
//
//         
//
//        示例 1：
//
//
//        输入：root = [3,9,20,null,null,15,7]
//        输出：2
//        示例 2：
//
//        输入：root = [2,null,3,null,4,null,5,null,6]
//        输出：5
//         
//
//        提示：
//
//        树中节点数的范围在 [0, 105] 内
//        -1000 <= Node.val <= 1000
//        通过次数364,028提交次数728,128
//
//        来源：力扣（LeetCode）
//        链接：https://leetcode-cn.com/problems/minimum-depth-of-binary-tree
//        著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。