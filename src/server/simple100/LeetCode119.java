package server.simple100;


import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * [119]杨辉三角2
 */
public class LeetCode119 {


    @Test
    public void test() {
        int numRows = 3;
        System.out.println(getRow(numRows));
    }

    public List<Integer> getRow(int rowIndex) {
        List<List<Integer>> list = new ArrayList<>();
        next(list, 0, ++rowIndex);
        return list.get(list.size() - 1);
    }


    public void next(List<List<Integer>> list, int index, int numRows) {
        if (index == numRows) {
            return;
        }
        List<Integer> newList = new ArrayList<>();
        for (int i = 0; i < index + 1; i++) {
            if (i == 0 || i == index) {
                newList.add(1);
            } else {
                List<Integer> frontRows = list.get(index - 1);
                Integer num = frontRows.get(i - 1) + frontRows.get(i);
                newList.add(num);
            }
        }
        list.add(newList);
        index++;
        next(list, index, numRows);
    }
}
