package server.simple100;

import org.junit.Test;

/***
 * 263. 丑数
 */
public class LeetCode263 {


    @Test
    public void test() {
        System.out.println(isUgly(456632323) );
    }


    public boolean isUgly(int n) {
        if (n < 1) {
            return false;
        }
        //
        while (true) {
            if (n % 2 == 0) {
                n = n / 2;
            } else if (n % 3 == 0) {
                n = n / 3;
            } else if (n % 5 == 0) {
                n = n / 5;
            } else {
                break;
            }
        }
        return n == 1;
    }

}
