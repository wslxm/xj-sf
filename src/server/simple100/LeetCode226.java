package server.simple100;

import org.junit.Test;

/***
 * 226. 翻转二叉树
 */
public class LeetCode226 {


    @Test
    public void test() {
        TreeNode treeNode1 = new TreeNode(1);
        TreeNode treeNode2 = new TreeNode(2);
        TreeNode treeNode3 = new TreeNode(3);
        treeNode2.left = treeNode1;
        treeNode2.right = treeNode3;
        TreeNode treeNode = invertTree(treeNode2);
        System.out.println("===========");
    }


    public TreeNode invertTree(TreeNode root) {
        // 2、递归遍历 (左 右 中)
        nextNode(root);
        return root;
    }


    void nextNode(TreeNode root) {
        if (root == null) {
            return;
        }
        TreeNode vRight = root.right;
        root.right = root.left;
        root.left = vRight;
        // 左 右 中
        // 1、遍历左节点
        nextNode(root.left);
        // 2、遍历右节点
        nextNode(root.right);
    }


    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
}
