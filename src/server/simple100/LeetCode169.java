package server.simple100;


import org.junit.Test;

import java.util.Arrays;


/**
 * 169. Excel表列名称
 */
public class LeetCode169 {


	@Test
	public void test() {
		int[] nums = {2, 2, 1, 1, 1, 2, 2};
		System.out.println(majorityElement(nums));
	}


	/**
	 * 解法2  先排序，从左到右 获取重复次数最多的最大值
	 *
	 * @param nums
	 * @return int
	 * @author wangsong
	 * @date 2022/4/8 13:31
	 */
	public int majorityElement(int[] nums) {
		// 计数
		Arrays.sort(nums);

		int maxNum = 0;
		int maxVal = 0;
		int resMaxNum = 0;
		int resMaxVal = 0;
		for (int i = 0; i < nums.length; i++) {
			if (maxVal != nums[i]) {
				maxVal = nums[i];
				maxNum = 1;
			} else {
				maxNum++;
			}
			if (maxNum > resMaxNum) {
				resMaxNum = maxNum;
				resMaxVal = maxVal;
			}
		}
		return resMaxVal;
	}


	/**
	 * 解法1  先计数 ，在获取计数器中的的最大值
	 *
	 * @param nums
	 * @return int
	 * @author wangsong
	 * @date 2022/4/8 13:30
	 */
//	public int majorityElement(int[] nums) {
//		// 计数
//		Map<Integer, Integer> map = new HashMap();
//		for (int i = 0; i < nums.length; i++) {
//			if (map.containsKey(nums[i])) {
//				map.put(nums[i], map.get(nums[i]) + 1);
//			} else {
//				map.put(nums[i], 1);
//			}
//		}
//		//
//		int max = 0;
//		int val = 0;
//		for (Integer key : map.keySet()) {
//			if (map.get(key) > max) {
//				max = map.get(key);
//				val = key;
//			}
//		}
//		return val;
//	}

}
