package server.simple100;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/***
 * 225. 用队列实现栈
 */
public class LeetCode225 {


    @Test
    public void test() {

        MyStack myStack = new MyStack();
        myStack.push(1);
        myStack.push(2);
        int top = myStack.top();// 返回 2
        int pop = myStack.pop();// 返回 2
        boolean empty = myStack.empty();// 返回 False

        System.out.println(top);
        System.out.println(pop);
        System.out.println(empty);
    }


    class MyStack {

        List<Integer> stack = null;

        public MyStack() {
            stack = new ArrayList<>();
        }

        public void push(int x) {
            stack.add(x);
        }

        public int pop() {
            int top = top();
            stack.remove(stack.size() - 1);
            return top;
        }

        public int top() {
            return stack.get(stack.size() - 1);
        }

        public boolean empty() {
           return stack.size() == 0;
        }
    }
}
