package server.simple100;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/***
 * 228. 汇总区间
 */
public class LeetCode228 {


    @Test
    public void test() {


        int[] nums = {0, 2, 3, 4, 6, 8, 9};
        System.out.println(summaryRanges(nums).toString());
    }


    public List<String> summaryRanges(int[] nums) {
        List<String> res = new ArrayList<>();
        int zindex = 0;
        for (int i = 0; i < nums.length; i++) {
            // 最后一位单独处理
            int numi = nums[i];
            if (i == nums.length - 1) {
                if (zindex == i) {
                    res.add(nums[i] + "");
                } else {
                    res.add(nums[zindex] + "->" + numi);
                }
            } else {
                int numj = nums[i + 1];
                if (numj - numi != 1) {
                    // 第一位
                    // 中间只有一位
                    // 正常中间数据
                    if (zindex == i) {
                        res.add(numi + "");
                    } else {
                        res.add(nums[zindex] + "->" + numi);
                    }
                    zindex = i + 1;
                }
            }
            //   [0,0] --> "0"
            //   [2,4] --> "2->4"
            //   [6,6] --> "6"
            //   [8,9] --> "8->9"
        }
        return res;
    }
}
