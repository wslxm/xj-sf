package server.simple100;

import org.junit.Test;

/***
 * 283. 移动零
 */
public class LeetCode283 {


    @Test
    public void test() {

       //
    }


    public void moveZeroes(int[] nums) {
        // 0 1 2 3 4 0 5 7 6
        // 1 2 3 4 5 7 6 0 0
        int zeroCount = 0;
        for (int i = 0; i < nums.length; i++) {
            // 前移
            if (nums[i] == 0) {
                zeroCount += 1;
            } else {
                nums[i - zeroCount] = nums[i];
            }
        }
        // 未位数置零 （出现过几次就置几位）
        for (int i = 0; i < zeroCount; i++) {
            nums[nums.length - 1 - i] = 0;
        }
    }
}
