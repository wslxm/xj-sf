package server.simple100;


import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/***
 * [118]杨辉三角
 */
public class LeetCode118 {

    //  输入: numRows = 5
    //  输出: [[1],[1,1],[1,2,1],[1,3,3,1],[1,4,6,4,1]]
    @Test
    public void test() {
        int numRows = 30;
        System.out.println(generate(numRows));
    }

    public List<List<Integer>> generate(int numRows) {
        List<List<Integer>> list = new ArrayList<>();
        next(list, 0, numRows);
        return list;
    }


    public void next(List<List<Integer>> list, int index, int numRows) {
        if (index == numRows) {
            return;
        }
        List<Integer> newList = new ArrayList<>();
        for (int i = 0; i < index + 1; i++) {
            if (i == 0 || i == index) {
                newList.add(1);
            } else {
                List<Integer> frontRows = list.get(index - 1);
                Integer num = frontRows.get(i - 1) + frontRows.get(i);
                newList.add(num);
            }
        }
        list.add(newList);
        index++;
        next(list, index, numRows);
    }
}
