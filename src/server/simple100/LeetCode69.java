package server.simple100;


import org.junit.Test;

/***
 * 69、x 的平方根
 *
 */
public class LeetCode69 {


    @Test
    public void test() {
        int x = 10;
        System.out.println(mySqrt(x));
    }


    public int mySqrt(int x) {
        // 左边界
        int left = 0;
        // 右边界
        int right = x;
        // 精确度, 最终如果没有相等的值, 获取临近的向左边界的第一个数
        int ans = -1;
        // 当最小值小于平分值,一直循环
        while (right >= left) {
            // 获取二分中值，防止溢出，计算公式为： 最小值 + (平分值-最小值)/2
            int mid = left + (right - left) / 2;
            if ((long) mid * mid <= x) {
                // 如果小于结果，设置临近值, 并且左边界向前移动一位,只到移动到靠近右边界的值(临近答案) 或找到最终结果为止(绝对答案)
                ans = mid;
                left = mid + 1;
            } else {
                // 如果大于结果，让右边界缩小一半
                right = mid - 1;
            }
        }
        return ans;
    }
}


// 实现 int sqrt(int x) 函数。
//
//        计算并返回 x 的平方根，其中 x 是非负整数。
//
//        由于返回类型是整数，结果只保留整数的部分，小数部分将被舍去。
//
//        示例 1:
//
//        输入: 4
//        输出: 2
//        示例 2:
//
//        输入: 8
//        输出: 2
//        说明: 8 的平方根是 2.82842...,
//             由于返回类型是整数，小数部分将被舍去。
//
//        来源：力扣（LeetCode）
//        链接：https://leetcode-cn.com/problems/sqrtx
//        著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。