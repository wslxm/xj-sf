package server.simple100;


import org.junit.Test;

import java.util.ArrayList;
import java.util.List;


/**
 * [125]验证回文串
 */
public class LeetCode125 {


    @Test
    public void test() {
        String s = "A man, a plan, a canal: Panaa";
        System.out.println(isPalindrome(s));
    }

    public boolean isPalindrome(String s) {
        // 字符范围
        String enstr = "qwertyuiopasdfghjklzxcvbnm1234567890";
        List<String> enList = new ArrayList<>();
        for (int i = 0; i < enstr.length(); i++) {
            enList.add(enstr.charAt(i) + "");
        }
        // 去除字符
        List<String> sList = new ArrayList<>();
        for (int i = 0; i < s.length(); i++) {
            String v = (s.charAt(i) + "").toLowerCase();
            if (enList.contains(v)) {
                sList.add(v);
            }
        }
        // 判断
        for (int i = 0; i < sList.size() / 2; i++) {
            if (!sList.get(i).equals(sList.get(sList.size() - 1 - i))) {
                return false;
            }
        }
        return true;
    }
}
