package server.simple100;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/***
 * 242. 有效的字母异位词
 */
public class LeetCode242 {


    @Test
    public void test() {
        String s = "aa", t = "a";
        System.out.println(this.isAnagram(s, t));
        ;
    }

    public boolean isAnagram(String s, String t) {
        List<Character> s1 = new ArrayList<>();
        List<Character> t1 = new ArrayList<>();
        for (int i = 0; i < s.length(); i++) {
            s1.add(s.charAt(i));
        }
        for (int i = 0; i < t.length(); i++) {
            t1.add(t.charAt(i));
        }
        Collections.sort(s1);
        Collections.sort(t1);
        return s1.toString().equals(t1.toString());
    }


}
