package server.simple100;


import org.junit.Test;


/**
 * 168. Excel表列名称
 */
public class LeetCode168 {


	@Test
	public void test() {
		System.out.println(convertToTitle(52));
	}


	public String convertToTitle(int columnNumber) {
		String sb = "";
		while (columnNumber > 0) {
			char yu = (char) ('A' + --columnNumber % 26 );
			sb = yu + sb;
			columnNumber = columnNumber / 26;
		}
		return sb;
	}


}
