package server.simple100;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/***
 * 205. 同构字符串
 */
public class LeetCode205 {


    @Test
    public void test() {
        System.out.println(this.isIsomorphic("bbbaaaab", "aaabbbba"));
    }


    public boolean isIsomorphic(String s, String t) {
        // 方法二：哈希表
        Map<String, String> smap = new HashMap<>();
        Map<String, String> tmap = new HashMap<>();
        for (int i = 0; i < s.length(); i++) {
            String x = s.charAt(i)+"";
            String y = t.charAt(i)+"";
            if(smap.containsKey(x) && !smap.get(x).equals(y)){
                return false;
            }
            if(tmap.containsKey(y) &&  !tmap.get(y).equals(x)){
                return false;
            }
            smap.put(x,y);
            tmap.put(y,x);
        }
        return true;
    }

}
