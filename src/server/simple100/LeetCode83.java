package server.simple100;


import org.junit.Test;

/***
 * 83. 删除排序链表中的重复元素
 */
public class LeetCode83 {


    @Test
    public void test() {
        // 构建链表数据
        ListNode listNode3 = new ListNode(1);
        ListNode listNode2 = new ListNode(1);
        ListNode listNode1 = new ListNode(1);
        // 调用
        ListNode listNode = deleteDuplicates(listNode1);
        System.out.println("");
    }


    public ListNode deleteDuplicates(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }
        ListNode headP = head;
        while (headP.next != null) {
            // 当前节点值
            int val = headP.val;
            // 下节点值
            int nextVal = headP.next.val;
            if (val == nextVal) {
                // 当前节点=下节点值, 让下下节点 为 当前节点的下节点, 遍历下节点(下节点已变跟为下下节点,当前节点不变)
                headP.next = headP.next.next;
            } else {
                // 不等于,遍历下节点, 当前节点向下走一
                headP = headP.next;
            }
        }
        return head;
    }


    class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }
}


//存在一个按升序排列的链表，给你这个链表的头节点 head ，请你删除所有重复的元素，使每个元素 只出现一次 。
//
//        返回同样按升序排列的结果链表。
//
//         
//
//        示例 1：
//
//
//        输入：head = [1,1,2]
//        输出：[1,2]
//        示例 2：
//
//
//        输入：head = [1,1,2,3,3]
//        输出：[1,2,3]
//         
//
//        提示：
//
//        链表中节点数目在范围 [0, 300] 内
//        -100 <= Node.val <= 100
//        题目数据保证链表已经按升序排列
//
//        来源：力扣（LeetCode）
//        链接：https://leetcode-cn.com/problems/remove-duplicates-from-sorted-list
//        著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。