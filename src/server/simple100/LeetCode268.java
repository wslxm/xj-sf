package server.simple100;

import org.junit.Test;

import java.util.Arrays;

/***
 * 268. 丢失的数字
 */
public class LeetCode268 {


    @Test
    public void test() {

        //System.out.println(missingNumber());
    }


    public int missingNumber(int[] nums) {
        Arrays.sort(nums);
        for (int i = 0; i < nums.length - 1; i++) {
            if (nums[i + 1] - nums[i] > 1) {
                return nums[i] + 1;
            }
        }
        if (nums[0] != 0) {
            return 0;
        } else {
            return nums[nums.length - 1] + 1;
        }
    }
}
