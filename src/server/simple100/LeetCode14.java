package server.simple100;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * 14. 最长公共前缀
 * @author wangsong
 * @mail 1720696548@qq.com
 * @date 2021/4/5 0005 22:17
 * @version 1.0.0
 */
public class LeetCode14 {


    @Test
    public void test() {
        String[] strs = {"dog","racecar","car"};
        System.out.println(longestCommonPrefix(strs));
    }

    public String longestCommonPrefix(String[] strs) {
        // 判空
        if (strs == null || strs.length == 0) {
            return "";
        }
        // 获取所有可能的最长前缀
        List<String> strsList = new ArrayList<>();
        for (int i = 0; i < strs[0].length(); i++) {
            strsList.add(strs[0].substring(0, i +1));
        }

        // 开始检查
        String resVal = "";
        // 遍历可能为最长前缀的数据
        for (int i = 0; i < strsList.size(); i++) {
            String val1 = strsList.get(i);
            // 遍历入参
            boolean isVal = true;
            for (int j = 0; j < strs.length; j++) {
                // 判断当前字符串的长度
                if (strs[j].length() < val1.length()) {
                    isVal = false;
                    break;
                }
                // 判断字符是否相等
                String val2 = strs[j].substring(0, val1.length());
                if (!val2.equals(val1)) {
                    isVal = false;
                }
            }
            // 判断结果
            if (isVal) {
                resVal = val1;
            } else {
                break;
            }
        }
        return resVal;
    }


//    public String longestCommonPrefix(String[] strs) {
//        if (strs == null || strs.length == 0) {
//            return "";
//        }
//        List<String> strsList = new ArrayList<>();
//        String s1 = strs[0] + "";
//        for (int i = s1.length(); i > 0; i--) {
//            strsList.add(s1.substring(0, i));
//        }
//        for (String s : strsList) {
//            boolean res = true;
//            for (int i = 0; i < strs.length; i++) {
//                if (strs[i].length() < s.length()) {
//                    res = false;
//                    break;
//                } else {
//                    String sv = strs[i].substring(0, s.length());
//                    if (sv.indexOf(s) == -1) {
//                        res = false;
//                        break;
//                    }
//                }
//            }
//            if (res) {
//                return s;
//            }
//        }
//        return "";
//    }
}


/**
 * 题目信息
 * <P>
 *编写一个函数来查找字符串数组中的最长公共前缀。
 *
 * 如果不存在公共前缀，返回空字符串 ""。
 *
 *  
 *
 * 示例 1：
 *
 * 输入：strs = ["flower","flow","flight"]
 * 输出："fl"
 * 示例 2：
 *
 * 输入：strs = ["dog","racecar","car"]
 * 输出：""
 * 解释：输入不存在公共前缀。
 *  
 *
 * 提示：
 *
 * 0 <= strs.length <= 200
 * 0 <= strs[i].length <= 200
 * strs[i] 仅由小写英文字母组成
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/longest-common-prefix
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
