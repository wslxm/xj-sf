package server.simple100;

import org.junit.Test;

import java.util.Arrays;

/***
 * 217. 存在重复元素
 */
public class LeetCode217 {


    @Test
    public void test() {

        int[] nums = {1, 2, 3, 2};
        boolean b = containsDuplicate(nums);
        System.out.println(b);
    }



    public boolean containsDuplicate(int[] nums) {
        // 1-先排序
        Arrays.sort(nums);
        for (int i = 0; i < nums.length - 1; i++) {
            if (nums[i] == nums[i + 1]) {
                return true;
            }
        }
        return false;
    }
}
