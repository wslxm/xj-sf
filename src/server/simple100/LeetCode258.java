package server.simple100;

import org.junit.Test;

/***
 * 258. 各位相加
 */
public class LeetCode258 {


    @Test
    public void test() {
        System.out.println(addDigits(100));
    }


    public int addDigits(int num) {
        while (num > 9) {
            // 每次循环对 num 进行拆分，然后相加
            String numStr = num + "";
            System.out.println(numStr);
            int newNum = 0;
            // 152
            for (int i = 0; i < numStr.length(); i++) {
                newNum += Integer.parseInt(numStr.charAt(i) + "");
            }
            num = newNum;
        }
        return num;
    }

}
