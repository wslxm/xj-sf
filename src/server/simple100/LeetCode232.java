package server.simple100;

import org.junit.Test;

import java.util.Stack;

/***
 *  232. 用栈实现队列
 */
public class LeetCode232 {


    @Test
    public void test() {
        MyQueue myQueue = new MyQueue();
        myQueue.push(1);   // queue is: [1]
        myQueue.push(2); // queue is: [1, 2] (leftmost is front of the queue)
        System.out.println(myQueue.peek()); // return 1
        System.out.println(myQueue.pop()); // return 1, queue is [2]
        System.out.println(myQueue.empty()); // return false


    }

    class MyQueue {

        Stack<Integer> stack = new Stack<>();
        Stack<Integer> stackTwo = new Stack<>();

        public MyQueue() {
        }

        public void push(int x) {
            stack.push(x);
        }

        public int pop() {
            while (!stack.isEmpty()) {
                Integer pop = stack.pop();
                stackTwo.push(pop);
            }
            Integer val = stackTwo.pop();
            while (!stackTwo.isEmpty()) {
                Integer pop = stackTwo.pop();
                stack.push(pop);
            }
            return val;
        }

        public int peek() {
            while (!stack.isEmpty()) {
                Integer pop = stack.pop();
                stackTwo.push(pop);
            }
            Integer val = stackTwo.peek();
            while (!stackTwo.isEmpty()) {
                Integer pop = stackTwo.pop();
                stack.push(pop);
            }
            return val;
        }

        public boolean empty() {
            return stack.empty();
        }
    }

}
