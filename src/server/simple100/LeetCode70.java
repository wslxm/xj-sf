package server.simple100;


import org.junit.Test;

/***
 * 70. 爬楼梯
 */
public class LeetCode70 {


    @Test
    public void test() {
        int x = 5;
        System.out.println(climbStairs(x));
    }


    public int climbStairs(int n) {
        int a = 1;
        int b = 2;
        if (n <= b) {
            return n;
        }
        while (n-- > 2) {
            int x2 = a + b;
            a = b;
            b = x2;
        }
        return b;
    }
}


//1 ==  1
//2 ==  2    1
//3 ==  3    1+2
//4 ==  5    2+3
//5 ==  8    3+5
//6 ==  13   5+8
//
//
//-- 为4时为5
//1111
//121
//112
//22
//211
//-- 为5时为8
//11111
//1112
//1121
//1211
//2111
//221
//212
//122


//假设你正在爬楼梯。需要 n 阶你才能到达楼顶。
//
//        每次你可以爬 1 或 2 个台阶。你有多少种不同的方法可以爬到楼顶呢？
//
//        注意：给定 n 是一个正整数。
//
//        示例 1：
//
//        输入： 2
//        输出： 2
//        解释： 有两种方法可以爬到楼顶。
//        1.  1 阶 + 1 阶
//        2.  2 阶
//        示例 2：
//
//        输入： 3
//        输出： 3
//        解释： 有三种方法可以爬到楼顶。
//        1.  1 阶 + 1 阶 + 1 阶
//        2.  1 阶 + 2 阶
//        3.  2 阶 + 1 阶
//
//        来源：力扣（LeetCode）
//        链接：https://leetcode-cn.com/problems/climbing-stairs
//        著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。