package server.simple100;


import org.junit.Test;

/***
 * 67. 二进制求和
 *  <P>
 *      二进制运算求和规则
 *      从末尾数开始相加， 加法：0+0=0；0+1=1；1+0=1；1+1=10；0进位为1。
 *
 *      如下：
 *      1010
 *      1011
 *     1 1    （逢2进1）
 *      ----
 *     10101
 *
 *  步骤：
 *  忘记二进制该怎么加了，ok，搞定
 *   1、把入参x1+ x2 和 返回结果 x3 设置为相同长度的字符串，长度不过默认填充值为0
 *   2、遍历 x1 计算并逐步替换 x3 的内容即可
 *  </P>
 */
public class LeetCode67 {


    @Test
    public void test() {
        String a = "1";
        String b = "111";
        System.out.println(addBinary(a, b));
    }


    public String addBinary(String a, String b) {
        // x1 len > x2 len 100%
        String x1 = a;
        String x2 = b;
        String x3 = "";
        // 设置x1, x2, x3长度相同, 不够前方补0，x3为计算结果,默认全0
        int len = a.length() - b.length();
        if (len > 0) {
            for (int i = 0; i < len; i++) {
                x2 = "0" + x2;
            }
        } else {
            for (int i = len; i < 0; i++) {
                x1 = "0" + x1;
            }
        }
        for (int i = 0; i < x1.length(); i++) {
            x3 += "0";
        }
        // 计算
        for (int i = x1.length() - 1; i >= 0; i--) {
            // 获取计算值
            int c1 = Integer.parseInt(x1.charAt(i) + "");
            int c2 = Integer.parseInt(x2.charAt(i) + "");
            // 获取进位值
            int c3 = Integer.parseInt(x3.charAt(i) + "");
            int num = c1 + c2 + c3;
            // 逢2进1
            if (num >= 2) {
                int yu = num % 2;
                if (i != 0) {
                    // 进1 取余
                    x3 = x3.substring(0, i - 1) + "1" + yu + x3.substring(i + 1);
                } else {
                    // 如果是最后一位数相加,直接进1 取余
                    x3 = "1" + yu + x3.substring(i + 1);
                }
            } else {
                // 不进位直接设置值,默认为0,不处理0
                if (num == 1) {
                    x3 = x3.substring(0, i) + "1" + x3.substring(i + 1);
                }
            }
        }
        return x3;
    }
}

//给你两个二进制字符串，返回它们的和（用二进制表示）。
//
//        输入为 非空 字符串且只包含数字 1 和 0。
//
//         
//
//        示例 1:
//
//        输入: a = "11", b = "1"
//        输出: "100"
//        示例 2:
//
//        输入: a = "1010", b = "1011"
//        输出: "10101"
//         
//
//        提示：
//
//        每个字符串仅由字符 '0' 或 '1' 组成。
//        1 <= a.length, b.length <= 10^4
//        字符串如果不是 "0" ，就都不含前导零。
//
//        来源：力扣（LeetCode）
//        链接：https://leetcode-cn.com/problems/add-binary
//        著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。