package server.simple100;


import org.junit.Test;


/**
 * 160. 相交链表
 */
public class LeetCode160 {


	@Test
	public void test() {
		ListNode treeNodeA = new ListNode(1);
		ListNode treeNodeB = new ListNode(2);
		//
//		ListNode treeNode3 = new ListNode(3);
//		ListNode treeNode4 = new ListNode(4);
//		ListNode treeNode5 = new ListNode(5);
//		ListNode treeNode6 = new ListNode(6);
//
//		treeNodeA.next = treeNode3;
//		treeNodeB.next = treeNode3;
//		treeNode3.next = treeNode4;
//		treeNode4.next = treeNode5;
//		treeNode5.next = treeNode6;
		System.out.println(getIntersectionNode( treeNodeA,  treeNodeA));	;
	}


	public ListNode getIntersectionNode(ListNode headA, ListNode headB) {

		// 解题思路 (使用双重遍历)
		// 遍历a 的每一个节点时， 遍历b节点的所有值，如果有相等，则找到并返回，否则返回null
		ListNode node = null;
		while (node == null && headA != null) {
			ListNode copyHeadB =  headB;
			while (node == null && copyHeadB != null) {
				if (headA == copyHeadB) {
					node = headA;
				}
				copyHeadB = copyHeadB.next;
			}
			headA = headA.next;
		}
		return node;
	}


	public class ListNode {
		int val;
		ListNode next;

		ListNode(int x) {
			val = x;
			next = null;
		}
	}
}
