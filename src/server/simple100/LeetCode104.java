package server.simple100;


import org.junit.Test;

/***
 * 104. 二叉树的最大深度
 */
public class LeetCode104 {

    //[0,2,4,1,null,3,-1,5,1,null,6,null,8] =4
    @Test
    public void test() {
        TreeNode root = new TreeNode(0,
                new TreeNode(2, new TreeNode(1, new TreeNode(5), new TreeNode(1)), null),
                new TreeNode(4, new TreeNode(3, null, new TreeNode(6)), new TreeNode(-1, null, new TreeNode(8))
                ));
//        TreeNode root = new TreeNode(0);
        System.out.println(maxDepth(root));
    }



    public int maxDepth(TreeNode root) {
        if(root == null) {
            return 0;
        } else {
            // 递归到最底层, 每次比较左右值,大的进行+1并返回
            int left = maxDepth(root.left);
            int right = maxDepth(root.right);
            return Math.max(left, right) + 1;
        }
    }





    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
}
//给定一个二叉树，找出其最大深度。
//
//        二叉树的深度为根节点到最远叶子节点的最长路径上的节点数。
//
//        说明: 叶子节点是指没有子节点的节点。
//
//        示例：
//        给定二叉树 [3,9,20,null,null,15,7]，
//
//        3
//        / \
//        9  20
//        /  \
//        15   7
//        返回它的最大深度 3 。
//
//        来源：力扣（LeetCode）
//        链接：https://leetcode-cn.com/problems/maximum-depth-of-binary-tree
//        著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。