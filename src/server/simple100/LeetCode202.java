package server.simple100;

import org.junit.Test;

/***
 * 202. 快乐数
 */
public class LeetCode202 {


    @Test
    public void test() {
        System.out.println(isHappy(2));
        ;
    }

    public boolean isHappy(int n) {
        return nextHappy(new int[]{n, n}, 2);
    }


    boolean nextHappy(int[] ns, int count) {
        for (int j = 0; j < count; j++) {
            String nStr = ns[count - 1] + "";
            Integer num = 0;
            for (int i = 0; i < nStr.length(); i++) {
                char charAt = nStr.charAt(i);
                int i1 = Integer.parseInt(charAt + "");
                num += i1 * i1;
            }
            if (num == 1) {
                return true;
            }
            ns[count - 1] = num;
        }
        if (ns[0] == ns[1]) {
            return false;
        }
        return nextHappy(ns, count == 1 ? 2 : 1);
    }
}
