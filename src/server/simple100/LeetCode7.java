package server.simple100;

import org.junit.Test;

/**
 * 7. 整数反转
 * @author wangsong
 * @mail 1720696548@qq.com
 * @date 2021/4/5 0005 22:17
 * @version 1.0.0
 */
public class LeetCode7 {


    @Test
    public void test() {
        int x = 123;
        System.out.println(reverse( x));
    }


    public int reverse(int x) {
        String xstr = x + "";
        String fh = "";
        if (x < 0) {
            xstr = xstr.substring(1);
            fh = "-";
        }

        String newxstr = fh + "";
        for (int i = xstr.length()-1; i >= 0; i--) {
            newxstr += xstr.charAt(i);
        }
        try {
            return Integer.parseInt(newxstr);
        } catch (Exception e) {
            return 0;
        }
    }
}


/**
 * 题目信息
 * <P>
 *
 * 给你一个 32 位的有符号整数 x ，返回将 x 中的数字部分反转后的结果。
 * 如果反转后整数超过 32 位的有符号整数的范围 [−231,  231 − 1] ，就返回 0。
 * 假设环境不允许存储 64 位整数（有符号或无符号）。
 *  
 *
 * 示例 1：
 *
 * 输入：x = 123
 * 输出：321
 * 示例 2：
 *
 * 输入：x = -123
 * 输出：-321
 * 示例 3：
 *
 * 输入：x = 120
 * 输出：21
 * 示例 4：
 *
 * 输入：x = 0
 * 输出：0
 *  
 *
 * 提示：
 *
 * -231 <= x <= 231 - 1
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/reverse-integer
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * </P>
 */
