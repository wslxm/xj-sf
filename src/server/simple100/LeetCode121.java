package server.simple100;


import org.junit.Test;


/**
 * [121]买卖股票的最佳时机
 */
public class LeetCode121 {


    //输入：[7,1,5,3,6,4]
//输出：5
//解释：在第 2 天（股票价格 = 1）的时候买入，在第 5 天（股票价格 = 6）的时候卖出，最大利润 = 6-1 = 5 。
//     注意利润不能是 7-1 = 6, 因为卖出价格需要大于买入价格；同时，你不能在买入前卖出股票。
//

    @Test
    public void test() {
        int[] prices = {2,4,1};
        System.out.println(maxProfit(prices));
    }

    public int maxProfit(int[] prices) {
        if (prices.length == 0) {
            return 0;
        }
        // 当前最小值
        int minPrice = prices[0];
        // 当前最大的滑动大小
        int hdPrice = 0;
        for (int i = 1; i < prices.length; i++) {
            if (prices[i] < minPrice) {
                // 更新最小值
                minPrice = prices[i];
            } else {
                // 更新最大滑动值
                int price = prices[i] - minPrice;
                if (price > hdPrice) {
                    hdPrice = price;
                }
            }
        }
        return hdPrice;
    }
}
