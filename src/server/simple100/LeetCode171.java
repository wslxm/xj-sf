package server.simple100;


import org.junit.Test;


/**
 * 168. Excel表列名称
 */
public class LeetCode171 {


	@Test
	public void test() {
		System.out.println(titleToNumber("FXSHRXW"));
	}


	/**
	 * @return int
	 * @author wangsong
	 * @date 2022/4/8 13:31
	 */
	public int titleToNumber(String columnTitle) {
		int val = 0;
		for (int i = 0; i < columnTitle.length(); i++) {
			char as = columnTitle.charAt(i);
			int z = as - 'A' + 1;
			for (int j = 0; j < columnTitle.length() - i-1; j++) {
				z = z * 26;
			}
			val += z;
		}
		return val;
	}
}
