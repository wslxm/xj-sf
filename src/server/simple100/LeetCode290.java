package server.simple100;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/***
 * 290. 单词规律
 */
public class LeetCode290 {


    @Test
    public void test() {

        String pattern = "accd", s = "dog cat dat bog";
        System.out.println(wordPattern(pattern, s));
    }


    public boolean wordPattern(String pattern, String s) {
        // 获取 s 首字母
        String[] sArr = s.split(" ");
        if (pattern.length() != sArr.length) {
            return false;
        }
        // System.out.println(t1);
        Map<Character, String> map1 = new HashMap<>();
        Map<String, Character> map2 = new HashMap<>();
        for (int i = 0; i < pattern.length(); i++) {
            char s1 = pattern.charAt(i);
            String t1 = sArr[i];
            if (map1.containsKey(s1) && !map1.get(s1).equals(t1)) {
                return false;
            }
            if (map2.containsKey(t1) && map2.get(t1) != s1) {
                return false;
            }
            map1.put(s1, t1);
            map2.put(t1, s1);
        }
        return true;
    }
}
