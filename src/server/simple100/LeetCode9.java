package server.simple100;

import org.junit.Test;

/**
 * 9. 回文数
 * @author wangsong
 * @mail 1720696548@qq.com
 * @date 2021/4/5 0005 22:17
 * @version 1.0.0
 */
public class LeetCode9 {


    @Test
    public void test() {
        int x = 121;
        System.out.println(isPalindrome( x));
    }


    public boolean isPalindrome(int x) {
        if (x < 0) {
            return false;
        }
        String st = x + "";
        int let = st.length();

        int z = let / 2;
        int stai = 0;
        int endi = let - 1;
        for (int i = 0; i < z; i++) {
            if (st.charAt(stai) != st.charAt(endi)) {
                return false;
            }
            stai = stai + 1;
            endi = endi - 1;
        }
        return true;
    }
}


/**
 * 题目信息
 * <P>
 *
 * 给你一个整数 x ，如果 x 是一个回文整数，返回 true ；否则，返回 false 。
 *
 * 回文数是指正序（从左向右）和倒序（从右向左）读都是一样的整数。例如，121 是回文，而 123 不是。
 *
 *  
 *
 * 示例 1：
 *
 * 输入：x = 121
 * 输出：true
 * 示例 2：
 *
 * 输入：x = -121
 * 输出：false
 * 解释：从左向右读, 为 -121 。 从右向左读, 为 121- 。因此它不是一个回文数。
 * 示例 3：
 *
 * 输入：x = 10
 * 输出：false
 * 解释：从右向左读, 为 01 。因此它不是一个回文数。
 * 示例 4：
 *
 * 输入：x = -101
 * 输出：false
 *  
 *
 * 提示：
 *
 * -231 <= x <= 231 - 1
 *  
 *
 * 进阶：你能不将整数转为字符串来解决这个问题吗？
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/palindrome-number
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * </P>
 */
