package server.simple100;

import org.junit.Test;

/***
 * 278. 第一个错误的版本
 */
public class LeetCode278 {


    @Test
    public void test() {
        Solution solution = new Solution();
        System.out.println(solution.firstBadVersion(5));  ;
    }

    public class VersionControl {
        Integer bad = 4;

        boolean isBadVersion(int version) {
            return version >= bad;
        }
    }

    public class Solution extends VersionControl {
        public int firstBadVersion(int n) {
//            System.out.println(isBadVersion(3));
//            System.out.println(isBadVersion(5));
//            System.out.println(isBadVersion(4));
            //  1 2 3 4 5 6 7 8 9
            //  1 2 3 4  |  5 6 7 8 9
            int l = 0;
            int r = n;
            while (true) {
                int mid = l + ((r - l) / 2);
                //  < 往左边(true) | > 往右边（ false）
                if (isBadVersion(mid)) {
                    r = mid;
                } else {
                    l = mid;
                }
                if (r - l <= 1) {
                    break;
                }
            }
            boolean badVersion = isBadVersion(l);
            return badVersion ? l : r;
        }
    }
}
