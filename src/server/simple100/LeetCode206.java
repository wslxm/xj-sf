package server.simple100;

import org.junit.Test;

/***
 * 203. 移除链表元素
 */
public class LeetCode206 {


    @Test
    public void test() {
        ListNode node = new ListNode(1);
        ListNode node2 = new ListNode(2);
        ListNode node3 = new ListNode(3);
        ListNode node4 = new ListNode(4);
        node.next = node2;
        node2.next = node3;
        node3.next = node4;
        System.out.println(reverseList(node));
    }

    public ListNode reverseList(ListNode head) {
        ListNode preHead = null;
        while (head != null) {
            ListNode listNode = new ListNode(head.val);
            listNode.next = preHead;
            preHead = listNode;
            head = head.next;
        }
        return preHead;
    }


    public class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }
}
