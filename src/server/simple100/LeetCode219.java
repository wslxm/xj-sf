package server.simple100;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/***
 * 219. 存在重复元素 II
 */
public class LeetCode219 {


    @Test
    public void test() {

        int[] nums = {1,0,1,1};
        boolean b = containsNearbyDuplicate(nums, 1);
        System.out.println(b);
    }


    public boolean containsNearbyDuplicate(int[] nums, int k) {
        // 1-先排序
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            int num = nums[i];
            if (map.containsKey(num) && i - map.get(num) <= k) {
                return true;
            }
            map.put(num, i);
        }
        return false;
    }
}
