package server.medium;

import org.junit.Test;

import java.util.Arrays;

/***
 * 167. 两数之和 II - 输入有序数组
 */
public class LeetCode167 {


	@Test
	public void test() {
		int[] numbers = {1, 3, 5, 7, 9, 12, 13, 18, 20, 50};
		System.out.println(Arrays.toString(twoSum(numbers, 70)));
		;
	}

	// 2+7 < 10  &&  11+15 >10
	// [2,7,11,15]   10
	public int[] twoSum(int[] numbers, int target) {
//		int l = 0;
//		int f = numbers.length - 1;
//		while (true) {
//			int mid = l + (f - l) / 2;
//			int lNumMax = numbers[l] + numbers[mid];
//			int fNumMax = numbers[mid + 1] + numbers[f];
//			if (lNumMax <= target && fNumMax >= target) {
//				// 获取到值的所在的索引范围段
//				System.out.println(l + "-" + f);
//				break;
//			} else if (lNumMax > target) {
//				// 往左
//				f = mid;
//			} else {
//				// 往右
//				l = mid;
//			}
//		}

		for (int i = 0, j = numbers.length - 1; i < j; ) {
			int sum = numbers[i] + numbers[j];
			if (sum == target) {
				return new int[]{i + 1, j + 1};
			} else if (sum > target) {
				j--;
			} else {
				i++;
			}
		}
		//
//		for (int i = l; i < f + 1; i++) {
//			for (int j = i + 1; j < f + 1; j++) {
//				if (numbers[i] + numbers[j] == target) {
//					return new int[]{i + 1, j + 1};
//				}
//			}
//		}
		return null;
	}
}
