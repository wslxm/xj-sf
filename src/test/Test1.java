package test;

public class Test1 {


    public static void main(String[] args) {
        Integer a = 1;
        Integer b = 2;
        System.out.println("交换前：a=" + a + ",b=" + b);
        swap(a, b);
        System.out.println("交换后：a=" + a + ",b=" + b);
    }

    private static void swap(Integer a, Integer b) {
        //  a 1  b 2
        //  a 2  b = 1
        //  int c = a;
        //  a = b;
        //  b = c;
        a++;
    }
}
