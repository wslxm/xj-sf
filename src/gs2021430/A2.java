package gs2021430;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class A2 {

    @Test
    public void test() {
        int[] prices = {4,3,2,7,8,2,3,1};
        System.out.println(findDisappearedNumbers(prices).toString());
    }


    public List<Integer> findDisappearedNumbers(int[] nums) {
        List<Integer> repList = new ArrayList<>();
        if(nums.length == 0){
            return repList;
        }
        int min = 1;
        int max = nums.length;
        for (int j = min; j <= max; j++) {
            boolean is = false;
            // 二分法
            for (int i = 0; i < nums.length; i++) {
               if(nums[i] == j){
                   is = true;
               }
            }
            if(!is){
                repList.add(j);
            }
        }
        return repList;
    }
}
