package gs2021430;

import org.junit.Test;

import java.util.Arrays;

/**
 * 商品折扣后的最终价格
 */
public class A1 {

    @Test
    public void test() {
        int[] prices = {10,1,1,6};
        System.out.println(Arrays.toString(finalPrices(prices)));
    }


    public int[] finalPrices(int[] prices) {
        if(prices.length == 0){
            return prices;
        }
        for (int i = 0; i < prices.length; i++) {
            for (int j = i + 1; j < prices.length; j++) {
                if (prices[j] <= prices[i]) {
                    prices[i] -= prices[j];
                    break;
                }
            }
        }
        return prices;
    }
}
