package gs2021430;

import org.junit.Test;

/**
 *
 */
public class A3 {

    @Test
    public void test() {
        int lowLimit = 1;
        int highLimit = 100000;
        System.out.println(countBalls(lowLimit, highLimit));
    }


    public int countBalls(int lowLimit, int highLimit) {
        if (lowLimit <= 0 || highLimit <= 0) {
            return 0;
        }
        // int n = highLimit - lowLimit + 1; 100000
        int[] nums = new int[9 * 5 + 1];
        for (int i = lowLimit; i <= highLimit; i++) {
            int index = 0;
            if (i >= 100000) {
                index = i / 100000 % 10 + i / 10000 % 10 + i / 1000 % 10 + i / 100 % 10 + i / 10 % 10 + i % 10;
            } else if (i >= 10000) {
                index = i / 10000 % 10 + i / 1000 % 10 + i / 100 % 10 + i / 10 % 10 + i % 10;
            } else if (i >= 1000) {
                index = i / 1000 % 10 + i / 100 % 10 + i / 10 % 10 + i % 10;
            } else if (i >= 100) {
                index = i / 100 % 10 + i / 10 % 10 + i % 10;
            } else if (i >= 10) {
                index = i / 10 % 10 + i % 10;
            } else {
                index = i % 10;
            }
            nums[index] += 1;
        }
        int max = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] > max) {
                max = nums[i];
            }
        }
        return max;
    }
}
